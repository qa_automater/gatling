import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._
import scala.language.postfixOps

/**
  * Created by sergey.lugovskoi on 13.04.2016.
  */
class Requests(var config: Config){

  val mainPage = {
    http("main page")
      .get("")
      .check(headerRegex("Set-Cookie", """JSESSIONID=(.*)""").saveAs("JSESSIONID"))
      .check(regex("""(\d{13,17})""").saveAs("afrLoop302"))
      .check(status.is(200))
  }

  val jessionid = {
    http("jsessionid")
      .get(";jsessionid=${JSESSIONID}")
      .queryParam("_afrLoop", "${afrLoop302}")
      .queryParam("_afrWindowMode", config.afrWindowMode)
      .queryParam("Adf-Window-Id", config.adfWindowId)
      .check(status.is(200))
  }

  val login = {
    http("login")
      .post("faces/login.jspx")
      .formParam("itUser", "${login}")
      .formParam("itPass", "gravca")
      .formParam("org.apache.myfaces.trinidad.faces.FORM", config.apache)
      .header("JSESSIONID","${JSESSIONID}")
      .check(status.is(200))
  }


}

