import com.typesafe.config.ConfigFactory
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._
import scala.language.postfixOps

class LoginSimulation extends Simulation {

  val config = new Config
  val requests = new Requests(config)

  val scn = scenario("login")
    .feed(config.feeder)
    .exec(requests.mainPage)
    .exec(requests.jessionid)
    .exec(requests.login)

  setUp(scn.inject(rampUsers(1) over (1 seconds)).protocols(config.httpConf))
}



