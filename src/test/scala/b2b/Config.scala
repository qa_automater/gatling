import com.typesafe.config.ConfigFactory
import io.gatling.core.Predef._
import io.gatling.http.Predef._

/**
  * Created by sergey.lugovskoi on 13.04.2016.
  */
class Config{

  val conf = ConfigFactory.load()
  val baseUrl = conf.getString("baseUrl")
  val agent = conf.getString("agent")
  val connection = conf.getString("connection")
  val langHeader = conf.getString("langHeader")
  val encodingHeader = conf.getString("encodingHeader")
  val users = conf.getString("usersFile")
  val apache = conf.getString("apache")
  val adfWindowId = conf.getString("adfWindowId")
  val afrWindowMode = conf.getString("afrWindowMode")

    
  val httpConf = http
    .baseURL(baseUrl)
    //.inferHtmlResources()
    .acceptEncodingHeader(encodingHeader)
    .acceptLanguageHeader(langHeader)
    .connection(connection)
    .userAgentHeader(agent)
    .disableCaching
    .disableWarmUp

  val feeder = csv(users).circular

  def init(){

  }

}
